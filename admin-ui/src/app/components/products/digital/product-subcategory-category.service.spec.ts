import { TestBed } from '@angular/core/testing';

import { ProductSubcategoryCategoryService } from './product-subcategory-category.service';

describe('ProductSubcategoryCategoryService', () => {
  let service: ProductSubcategoryCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductSubcategoryCategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
