import { Component, OnInit } from '@angular/core';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { FormBuilder,FormGroup,FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductSubcategoryCategoryService } from '../product-subcategory-category.service';
import { VarientService } from '../../varient/varient/varient.service';
@Component({
  selector: 'app-digital-add',
  templateUrl: './digital-add.component.html',
  styleUrls: ['./digital-add.component.scss']
})
export class DigitalAddComponent implements OnInit {

  constructor(private fb: FormBuilder,private _service :ProductSubcategoryCategoryService,private router : Router,private route : ActivatedRoute,private _service2 : VarientService) 
  {
    this.productData = new FormGroup({
      name : new FormControl('',Validators.required),
      description : new FormControl('',Validators.required),
      price : new FormControl('',Validators.required),
      stock : new FormControl('',Validators.required),
      // ids : new FormControl(Number,Validators.required),
      // id : new FormControl(Number,Validators.required)
    });
    this.getVarients();
  }
  varientIds = [1]
  subcategoryId = 5;
  varients;
  public config1: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 1,
    autoReset: null,
    errorReset: null,
    cancelReset: null
  };

  public onUploadInit(args: any): void { }

  public onUploadError(args: any): void { }

  public onUploadSuccess(args: any): void { }

  ngOnInit():void {
    let id = this.route.snapshot.params.productId;
    this.productId = id;
    console.log(id);
    debugger
    if(this.productId!=null) {
      this.getData(id);
      console.log(this.product);
      
      }
  }
  file:File = null;
  productData;
  productId
  product;
  selectFile(event) {
    this.file= <File>event.target.files[0];
    console.log(this.file)
  }
  formData = new FormData();
  saveProduct() {
    this.formData.append('multipartFile',this.file,this.file.name)
    this.formData.append('name',this.productData.value.name),
    this.formData.append('description',this.productData.value.description),
    this.formData.append('stock',this.productData.value.stock)
    // this.formData.append('',this.productData.value.name),
    // this.formData.append('',this.productData.value.name),
    // console.log(this.studentData.value)
    debugger;
   
    console.log(this.productData.value)
    if(this.productId==null) {
    this._service.addProduct(this.varientIds,this.subcategoryId,this.formData).subscribe((result=>{
console.log(result),error => {
      
  console.log(error);
  
}
;
this.router.navigate(['/products/varient-list'])
    }))
    // console.log(this.varientData.value)
  }else{
    this._service.updateProduct(this.varientIds,this.subcategoryId,this.formData).subscribe(data => {
      console.log(data)
      this.router.navigate(['/products/varient-list'])
    },error => {
      
      console.log(error);
      
    })
  }
  }

  getVarients() {
    this._service2.getAllVarient().subscribe(res => {
      this.varients = res ;
      console.log(this.varients)
    },error => {

    })
  }
  getData(id){
    this._service.getProduct(id).subscribe(res => {
      this.product=res;
      console.log(this.product.image);
      debugger
      console.log(this.product)
      if(this.product!=null) {
        this.productData.patchValue({
          name : this.product.name,
          description : this.product.description,
          price : this.product.price,
          stock : this.product.stock,
        });
    }
    },error => {
      
      console.log(error);
      
    })
  }
}

