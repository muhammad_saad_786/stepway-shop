import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductSubcategoryCategoryService {

  constructor(private http: HttpClient) {

  }
  url = "http://localhost:8080/product"
  url2 = "http://localhost:8080/subcategory"
  addProduct(ids, id, product) {
    return this.http.post(`${this.url}?ids=${ids}&id=${id}`, product)
  }
  updateProduct(ids, id, product) {
    return this.http.put(`${this.url}?ids=${ids}&id=${id}`, product)
  }
  getProduct(id) {
    return this.http.get(`${this.url}/${id}`)
  }
}
