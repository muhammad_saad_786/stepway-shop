package com.ecommerce.EcommerceBackend.Service;

import com.ecommerce.EcommerceBackend.Dto.ProductDto;
import com.ecommerce.EcommerceBackend.Model.CustomConstants;
import com.ecommerce.EcommerceBackend.Model.Product;
import com.ecommerce.EcommerceBackend.Model.SubCategory;
import com.ecommerce.EcommerceBackend.Model.Varient;
import com.ecommerce.EcommerceBackend.Repository.ProductRepository;
//import com.ecommerce.EcommerceBackend.Repository.SubCategoryRepository;
import com.ecommerce.EcommerceBackend.Repository.VarientRepository;
import com.ecommerce.EcommerceBackend.Shared.ApiResponse;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private VarientService varientService;
    @Value("${category.image.url}")
    String categoryImageUrl;
    @Autowired
    private VarientRepository varientRepository;
//    @Autowired
//    SubCategory subCategory;

    @Autowired
    SubCategoryService subCategoryService;
//
    public List<Product> getAllProducts(Long subCategoryId){
//        Optional<Varient> varient = varientService.findVarientById(id);
        List <Product> products = productRepository.findAll();
        List<Product> products1 = new ArrayList<>();
        for(Product pr : products) {
            if(pr.getSubCategory().getId().equals(subCategoryId)) {
                products1.add(pr);
            }
        }
        return products1;

    }
    public Product findProductById(Long id){
//        List<Product> products = this.getAllProducts(varientId);
//        Optional<Varient> varient = varientService.findVarientById(varientId);
//        Product product = new Product();
//        product.setVarient(varient.orElseThrow());
//        product = productRepository.findAll().stream().filter(c -> Long.valueOf(c.getId()) == id).findAny().get();
        Product product = productRepository.findById(id).orElseThrow(null);
        return product;
    }
//    public Product findProductById(Long id,Long varientId) {
//        Varient varient = new Varient();
//        productRepository.delete(productRepository.findAll().stream().filter(c -> Long.valueOf(c.getId()) == id
//                ).findAny().get());
//    }

    public void addProduct(List<Long> ids, Long id,ProductDto productDto) throws IOException {
        String unique = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        if(saveCategoryImage(productDto.getMultipartFile(),productDto.getName(),unique)){

            Product product = new Product();
            product.setImage(categoryImageUrl+"/"+unique+productDto.getMultipartFile().getOriginalFilename());
            product.setName(productDto.getName());
            getCategoryImage(unique+productDto.getMultipartFile().getOriginalFilename());
//            L
//            for(Long v : ids) {
//                product.getVarients().add(varientService.findVarientById(v));
//            }
//            category.setActive(true);
            productRepository.save(product);
//            return new ApiResponse(Status.Status_Ok,CustomConstants.CAT_POSTED,category);
        }

//        Varient varient = new Varient();
//        varient.setId(id);
//        product1.setVarient(varient);
//
//        product1.setSubCategory(product.getSubCategoryId());
//        product1.setSubCategory(
//                subCategoryRepository.findAll().stream().filter(x->Long.valueOf(x.getId())==subCategoryId).findAny().get()
//        );
//        productRepository.save(product);
//        System.out.println(s+ File.separator+productDto.getMultipartFile().getOriginalFilename());
    }

    public void updateProduct(List<Long> ids, Long id,ProductDto productDto){
        Product product= productRepository.findById(id).orElseThrow(null);
        String unique = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        if(saveCategoryImage(productDto.getMultipartFile(),productDto.getName(),unique)){

            Product category = new Product();

            category.setImage(categoryImageUrl+productDto.getName()+"/"+unique+productDto.getMultipartFile().getOriginalFilename());
//            category.setName(categoryDTO.getName());
//            category.setActive(true);
//            categoryRepository.save(category);
//            return new ApiResponse(Status.Status_Ok,CustomConstants.CAT_POSTED,category);
        }
//            category.setActive(true);
            productRepository.save(product);
//            return new ApiResponse(Status.Status_Ok,CustomConstants.CAT_POSTED,category);


//        product.setName(productDto.getName());
//        product.setVarients(productDto.getVarients());
//        product.setSubCategory(productDto.getSubCategory());
//        product.setPrice(productDto.getPrice());
//        product.setDescription(productDto.getDescription());
//        product.setImage(productDto.getImage());
//        product.setStock(productDto.getStock());
//        productRepository.save(product);
////        product1.setId(id);
//        product1.setName(product.getName());
//        product1.setDescription(product.getDescription());
//        Varient varient = new Varient();
//        varient.setId(varientId);
////        product1.setVarient(varient);
//        product1.setPrice(product.getPrice());
//        product1.setStock(product.getStock());
//        productRepository.saveAndFlush(product1);

    }
    public void deleteProductById(Long id){
//        productRepository.delete(productRepository.findAll().stream().filter(c -> Long.valueOf(c.getId()) == id).findAny().get());
        productRepository.delete(this.findProductById(id));
    }

    public Boolean saveCategoryImage(MultipartFile file, String name, String unique){
        try {

            String UPLOADED_FOLDER_NEW = CustomConstants.SERVER_PATH+"//"+"serverFiles//";

            File dir = new File(UPLOADED_FOLDER_NEW);
            dir.setExecutable(true);
            dir.setReadable(true);
            dir.setWritable(true);

            if(!dir.exists()){
                dir.mkdirs();
            }

            BufferedImage inputImage = ImageIO.read(file.getInputStream());

            BufferedImage resized = resize(inputImage, 30, 30);

            String format = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
            ImageIO.write(resized, format, new File(UPLOADED_FOLDER_NEW + unique+ file.getOriginalFilename()));

        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private BufferedImage resize(BufferedImage img, int height, int width) {
        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }
    public ResponseEntity<InputStreamResource> getCategoryImage(String filename) throws IOException{


        String filepath = CustomConstants.SERVER_PATH+"//"+"serverFiles//"+filename;

        File f = new File(filepath);
        Resource file = new UrlResource(f.toURI());
        System.out.println(file);
        return ResponseEntity
                .ok()
                .contentLength(file.contentLength())
                .contentType(
                        MediaType.parseMediaType("image/JPG"))
                .body(new InputStreamResource(file.getInputStream()));
    }
}
